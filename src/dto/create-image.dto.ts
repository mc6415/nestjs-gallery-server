export class CreateImageDTO {
    readonly title: string;
    readonly filename: string;
    readonly author: string;
    readonly date_posted: string;
    readonly data: Buffer;
    readonly thumbnail: Buffer;
    readonly user: string;
}