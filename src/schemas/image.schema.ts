import { Schema } from 'mongoose';

export const ImageSchema = new Schema({
    filename: String,
    author: String,
    title: String,
    date_posted: String,
    data: Buffer,
    thumbnail: Buffer,
    user: String,
});