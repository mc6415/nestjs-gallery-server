import { Document } from 'mongoose';

export interface Image extends Document {
    readonly title: string;
    readonly author: string;
    readonly filename: string;
    readonly date_posted: string;
    readonly env: string;
    readonly thumbnail: Buffer;
    readonly user: string;
}