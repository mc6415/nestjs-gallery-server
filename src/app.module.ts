import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ImageModule } from './image/image.module';

@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost/gallery', { useNewUrlParser: true}), ImageModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
