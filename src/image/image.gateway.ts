import { WebSocketGateway, OnGatewayConnection, OnGatewayDisconnect, WebSocketServer, SubscribeMessage } from '@nestjs/websockets';

@WebSocketGateway()
export class ImageGateway implements OnGatewayConnection, OnGatewayDisconnect {
    @WebSocketServer() server;
    users: number = 0;

    async handleConnection() {
        this.users++;

        console.log('connected');

        this.server.emit('users', this.users);
    }

    async handleDisconnect() {
        this.users--;

        this.server.emit('users', this.users);
    }
    
    async updatedImages() {
        this.server.emit('imagesUpdated');
    }

    @SubscribeMessage('chat')
    async onChat(client, message) {
        client.broadcast.emit('chat', message);
    }
}