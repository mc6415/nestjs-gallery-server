import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Image } from 'src/interfaces/image.interface';
import { CreateImageDTO } from 'src/dto/create-image.dto';

@Injectable()
export class ImageService {
    constructor(@InjectModel('Image') private readonly imageModel: Model<Image>) {}

    async addImage(createImageDTO: CreateImageDTO): Promise<Image> {
        const newImage = await new this.imageModel(createImageDTO);
        return newImage.save();
    }

    async getImages(): Promise<Image[]> {
        const images = await this.imageModel.find().exec();
        return images;
    }

    async deleteImage(id): Promise<any> {
        const image = await this.imageModel.findByIdAndRemove(id);

        return image;
    }
    
    async getEnvImages(env): Promise<Image[]> {
        const images = await this.imageModel.find({
            env
        }).exec();

        return images;
    }
}
