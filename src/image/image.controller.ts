import { Controller, Get, Res, HttpStatus, Post, UseInterceptors, UploadedFile, Body, Req } from '@nestjs/common';
import { ImageService } from './image.service';
import { ImageGateway } from './image.gateway';
import { CreateImageDTO } from 'src/dto/create-image.dto';
import { diskStorage } from 'multer';
import { extname, resolve } from 'path';
import { FileInterceptor } from '@nestjs/platform-express';
import { readFile } from 'fs';

@Controller('image')
export class ImageController {
    constructor(private imageService: ImageService, private imageGateway: ImageGateway) { }

    // Get all images
    @Get('images')
    async getImages(@Res() res) {
        const images =  await this.imageService.getImages();
        return res.status(HttpStatus.OK).json(images);
    }

    @Get('thumbs')
    async getThumbs(@Res() res) {
        const images = await this.imageService.getImages();

        let thumbs = images.map(image => {
            return {
                _id: image._id,
                thumbnail: image.thumbnail,
                title: image.title,
                filename: image.filename,
                author: image.author,
                date_posted: image.date_posted,
                uid: image.user
            }
        })

        return res.status(HttpStatus.OK).json(thumbs);
    }

    @Post('/delete')
    async deleteImage(@Body() body, @Res() res) {
        let { id } = body;

        const image = await this.imageService.deleteImage(id);

        await this.imageGateway.updatedImages();

        return res.status(HttpStatus.OK).json(image);
    }

    // Upload Image
    @Post('/upload')
    @UseInterceptors(FileInterceptor('file'))
    async addImage(@UploadedFile() file, @Body() body, @Res() res) {
        const imageThumbnail = require('image-thumbnail');
        let options = { percentage: 35 };
        const thumbnail = await imageThumbnail(file.buffer, options);

        console.log(file);

        console.log(body);

        let image = {
            title: body.title,
            filename: file.originalname,
            author: body.author,
            date_posted: new Date().toISOString(),
            data: file.buffer,
            thumbnail,
            user: body.user,
        };

        let newImage = await this.imageService.addImage(image);

        await this.imageGateway.updatedImages();

        return res.status(HttpStatus.OK).json({
            message: 'Image added!'
        });
    }
}
