import { Module } from '@nestjs/common';
import { ImageService } from './image.service';
import { ImageController } from './image.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ImageSchema } from 'src/schemas/image.schema';
import { ImageGateway } from './image.gateway';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Image', schema: ImageSchema }])
  ],
  providers: [ImageService, ImageGateway],
  controllers: [ImageController]
})
export class ImageModule {}
